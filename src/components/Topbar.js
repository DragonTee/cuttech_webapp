import React from "react";
import "../styles.css";
import phone from "../images/bx_bx-phone-call.svg";
import user from "../images/user.svg";
import basket from "../images/basket.svg";

function TopBar() {
  
  return (
    
    <div className="flex flex-row justify-between text-montserrat bg-p-dark h-[44px] text-white px-[375px] text-2xl">
        <div className="h-full flex items-center gap-1">
            <img src={phone} alt="phone"/>
            8-800-55-53-535
        </div>
        <div className="h-full flex items-center gap-4">
            <a className="hover:text-[#6B48CA] flex items-center gap-1" href="#">Войти<img src={user} alt="user"/></a>
            <a href="#">
                <img src={basket} alt="basket"/> 
            </a>
        </div>
    </div>
  );
}

export default TopBar;
