import React from "react";
import star from "../images/star.svg";
import starH from "../images/star_half.svg";

const ProductCard = ({name, price, rating, image, id}) => {
  
    var link = "/products/"+id;
    return (
      <>
        <a href={link} className="hover:scale-110 ease-in-out transition-all w-[360px] h-[400px] flex flex-col text-montserrat text-2xl" style={{background: "linear-gradient(132.36deg, #F6F1FF -223.52%, #F8EEFF 411.48%)"}}>
            <img src={image} alt="img" className="h-[280px] object-contain p-4"/>
            <div className="flex-grow bg-white">
                <div className="text-center w-full h-[4rem]">
                    {name}
                </div>
                <div className="flex flex-row justify-between px-4">
                    <div>
                        {price}₽
                    </div>
                    <div className="flex flex-row gap-1">
                    <img src={star} alt="star"/>
                    <img src={star} alt="star"/>
                    <img src={star} alt="star"/>
                    <img src={star} alt="star"/>
                    <img src={starH} alt="star"/>
                        {rating} 
                    </div>
                </div>
            </div>
        </a>
      </>
    );
  }
  
  export default ProductCard;