import React from "react";
import "../styles.css";
import phone from "../images/bx_bx-phone-call.svg";
import logo from "../images/Logo.svg";
import search from "../images/search.svg";
import TopBar from "./Topbar";

function Navbar() {
  
  return (
    <>
      <TopBar />
      <div className="flex flex-row justify-between text-montserrat bg-white h-[80px] text-black px-[375px] text-2xl">
          <a href="/" className="h-full flex items-center gap-1 hover:scale-110 transition-all ease-in-out">
              <img src={logo} alt="phone"/>
          </a>
          <div className="flex items-center gap-4">
            <ul className="flex items-center flex-row gap-[27px]">
              <a href="/" className="hover:text-[#6B48CA] hover:scale-110 transition-all ease-in-out"> 
                Главная
              </a>
              <a href="/products"  className="hover:text-[#6B48CA] hover:scale-110 transition-all ease-in-out">
                Все товары
              </a>
              <a href="/categories" className="hover:text-[#6B48CA] hover:scale-110 transition-all ease-in-out">
                Категории
              </a>
            </ul>
            <div className="w-[340px] h-[40px] border-2 border-[#D2D1D1] flex flex-row justify-between">
              <input type="text" placeholder="Найти..." className="flex-grow"></input>
              <div className="h-[36px] w-[36px] bg-p-light flex align-middle items-center justify-center">
                <img src={search} alt="phone" className="w-[20px] h-[20px]"/>
              </div>
            </div>
          </div>
      </div>
    </>
  );
}

export default Navbar;
