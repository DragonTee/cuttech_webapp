import React from "react";

const CategoryCard = ({name, image, id}) => {
  
    var preparedId = encodeURIComponent(id);
    var link = "/products/?category="+id;
    return (
      <>
        <a href={link} className="hover:scale-110 ease-in-out transition-all w-[360px] h-[400px] flex flex-col text-montserrat-medium text-2xl" style={{background: "linear-gradient(132.36deg, #F6F1FF -223.52%, #F8EEFF 411.48%)"}}>
            <img src={image} alt="img" className="h-[280px] object-contain p-4"/>
            <div className="flex-grow bg-white">
                <div className="text-center w-full h-[4rem]">
                    {name}
                </div>
            </div>
        </a>
      </>
    );
  }
  
  export default CategoryCard;