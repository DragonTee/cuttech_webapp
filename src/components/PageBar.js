import React from "react";
import "../styles.css";

const PageBar = ({text}) => {
  
  return (
    <>
      <div className="items-center w-full h-[233px] text-black px-[375px] text-[40px] font-roboto flex" style={{background: "linear-gradient(91.48deg, #FFFFFF -39.54%, rgb(107 72 202 / 42%) 162.12%)"}}>
            {text}
      </div>
    </>
  );
}

export default PageBar;
