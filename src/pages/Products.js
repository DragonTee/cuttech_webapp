import React, { useEffect, useState } from 'react';
import { useSearchParams } from 'react-router-dom';
import PageBar from '../components/PageBar';
import ProductCard from '../components/ProductCard';

function Products() {

      const [error, setError] = useState(null);
      const [isLoaded, setIsLoaded] = useState(false);
      const [items, setItems] = useState([]);
      console.log(items);
      const [searchParams, setSearchParams] = useSearchParams();
      var currentPage = 0;
      const pageSize = 6;
      
      const loadMore = () => {
        if (window.innerHeight + document.documentElement.scrollTop + 1 === document.scrollingElement.scrollHeight) {
            currentPage++;
            var preparedCategpryId = '';
            var category = searchParams.get("category");
            if (category != undefined)
                preparedCategpryId = "&category="+encodeURIComponent(category);
            fetch("http://127.0.0.1:8000/api/products/?page="+currentPage+"&pageSize="+pageSize+preparedCategpryId, {
                method: "GET",
            })
            .then(res => res.json())
            .then(
                (result) => {
                setIsLoaded(true);
                setItems((prevState) => {
                    console.log("result",result);
                    console.log("prev",prevState);
                    var res = prevState;
                    for (var i in result) {
                    res = res.concat(result[i]);
                    }
                    return res;
                    
                });
                },
                // Примечание: важно обрабатывать ошибки именно здесь, а не в блоке catch(),
                // чтобы не перехватывать исключения из ошибок в самих компонентах.
                (error) => {
                setIsLoaded(true);
                setError(error);
                }
            )
        }
    }
    
      // Примечание: пустой массив зависимостей [] означает, что
      // этот useEffect будет запущен один раз
      // аналогично componentDidMount()
      useEffect(() => {
        window.addEventListener('scroll', loadMore);
        var preparedCategpryId = '';
            var category = searchParams.get("category");
            if (category != undefined)
                preparedCategpryId = "&category="+encodeURIComponent(category);
            fetch("http://127.0.0.1:8000/api/products/?page="+currentPage+"&pageSize="+pageSize+preparedCategpryId, {
                method: "GET",
            })
          .then(res => res.json())
          .then(
            (result) => {
              setIsLoaded(true);
              setItems(result);
              console.log(items)
            },
            // Примечание: важно обрабатывать ошибки именно здесь, а не в блоке catch(),
            // чтобы не перехватывать исключения из ошибок в самих компонентах.
            (error) => {
              setIsLoaded(true);
              setError(error);
            }
          )
      }, []);
      var renderedData;
      if (error) {
        renderedData = <div>Ошибка: {error.message}</div>;
      } else if (!isLoaded) {
        renderedData = <div>Загрузка...</div>;
      } else {
        renderedData = (
          <>
            {items.map(item => (
              <ProductCard name={item.name} price={item.price} rating={4.5} image={item.image_url} id={item.id} ket={item.id}>
              </ProductCard>
            ))}
          </>
        );
      }
    return (
        <>
        <PageBar text={"Каталог товаров"}/>
        <div className='px-[375px] h-[122px] font-roboto text-4xl flex items-center'>
            Комплектующие для ПК
        </div>
        <div className='px-[375px] flex flex-row flex-wrap justify-evenly gap-y-10'>
            {renderedData}
        </div>
        </>
    );
}


export default Products;