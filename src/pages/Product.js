import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import PageBar from '../components/PageBar';
import ProductCard from '../components/ProductCard';
import basket from "../images/basket_1.svg";

const ProductDetails = ({description, specifications}) => {
  const [selectedSection, setSelectedSection] = useState(0);
  var underlined = selectedSection == 0 ? "underline" : "";
  var rendered;
  switch (selectedSection)
  {
    case 0:
      rendered = <div>{description}</div>;
      break;
    case 1:
      rendered = (
        <>
        {specifications.map(spec => (
          <div className='w-full flex flex-row gap-4 text-xl'>
            <div className='w-[300px]'>{spec.name}</div>
            <div>{spec.text}</div>
          </div>
        ))}
        </>
      );
      break;
    case 2:
      break;
  } 
  return (
    <>
  <div className='w-full flex flex-row h-[72px] gap-16 text-montserrat-medium text-[32px] items-center mt-8'>
    <div onClick={() => setSelectedSection(0)} className={(selectedSection == 0 ? "underline" : "")+" hover:cursor-pointer"}>
      Описание
    </div>
    <div onClick={() => setSelectedSection(1)} className={(selectedSection == 1 ? "underline" : "")+" hover:cursor-pointer"}>
      Технические характеристики
    </div>   
  </div>
  <div className='text-montserrat-medium'>{rendered}</div>
  </>
  );
}

function Product() {
    let { id } = useParams();
    const [error, setError] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);
    const [item, setItems] = useState([]);
    const [spec_error, setErrorSpec] = useState(null);
    const [spec_isLoaded, setIsLoadedSpec] = useState(false);
    const [spec_items, setItemsSpec] = useState([]);

    
      // Примечание: пустой массив зависимостей [] означает, что
      // этот useEffect будет запущен один раз
      // аналогично componentDidMount()
      useEffect(() => {
        let urlid = encodeURIComponent(id);
        fetch("http://127.0.0.1:8000/api/products/"+urlid, {
            method: "GET",
          })
          .then(res => res.json())
          .then(
            (result) => {
              setIsLoaded(true);
              setItems(result);
            },
            // Примечание: важно обрабатывать ошибки именно здесь, а не в блоке catch(),
            // чтобы не перехватывать исключения из ошибок в самих компонентах.
            (error) => {
              setIsLoaded(true);
              setError(error);
            }
          );
          fetch("http://127.0.0.1:8000/api/products/"+urlid+"/specifications/", {
            method: "GET",
          })
          .then(res => res.json())
          .then(
            (result) => {
              setIsLoadedSpec(true);
              setItemsSpec(result);
            },
            // Примечание: важно обрабатывать ошибки именно здесь, а не в блоке catch(),
            // чтобы не перехватывать исключения из ошибок в самих компонентах.
            (error) => {
              setErrorSpec(true);
              setItemsSpec(error);
            }
          )
      }, []);
      var renderedData;
      if (error) {
        renderedData = <div>Ошибка: {error.message}</div>;
      } else if (!isLoaded) {
        renderedData = <div>Загрузка...</div>;
      } else {
        var description = item.description && item.description.length > 300 ? item.description.substr(0, 300 - 1) + "..." : item.description;
        renderedData = (
          <div className='flex flex-row h-[500px] gap-8'>
            <img src={item.image_url} alt="img" className="hover:scale-125 transition-all ease-in-out hover:cursor-zoom-in h-full w-[450px] object-contain p-4" style={{background: "linear-gradient(132.36deg, #F6F1FF -223.52%, #F8EEFF 411.48%)"}}/>
            <div className='flex flex-col h-full flex-grow text-montserrat-medium'>
              <div className='text-4xl min-h-[4rem]'>
                {item.name}
              </div>
              <div className='text-xl'>
                {item.rating}
              </div>
              <div className='text-2xl'>
                {item.price}₽
              </div>
              <div className='text-2xl text-ellipsis h-[16rem]'>
                {description}
              </div>
              <div className='h-full flex flex-row justify-end items-end text-p-light text-4xl gap-2'>
                <a href='#' className='hover:underline flex flex-row gap-2'> 
                  <div className='h-[41px]'>Добавить в корзину</div>
                  <img src={basket} alt="basket" className=''/> 
                </a>
              </div>
            </div>
          </div>
        );
      }
      var specificationsRender;
      if (spec_error) {
        specificationsRender = <div>Ошибка: {error.message}</div>;
      } else if (!spec_isLoaded) {
        specificationsRender = <div>Загрузка...</div>;
      } else {
        var description = item.description && item.description.length > 300 ? item.description.substr(0, 300 - 1) + "..." : item.description;
        specificationsRender = (          
            <ProductDetails description = {item.description} specifications = {spec_items} />
        );
      }
    return (
        <>
        <PageBar text={"Описание товара"}/>
        <div className='px-[375px] pt-12'>
            {renderedData}
            
        </div>
        <div className='px-[375px] w-full h-[372px]' style={{background: "linear-gradient(91.48deg, rgba(255, 255, 255, 0.31) -39.54%, rgb(107 72 202 / 42%) 162.12%)"}}>
          {specificationsRender}
        </div>
        </>
    );
}

export default Product;