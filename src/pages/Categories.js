import React, { useEffect, useState } from 'react';
import CategoryCard from '../components/CategoryCard';
import PageBar from '../components/PageBar';
import ProductCard from '../components/ProductCard';

function Categories() {

      const [error, setError] = useState(null);
      const [isLoaded, setIsLoaded] = useState(false);
      const [items, setItems] = useState([]);
    
      // Примечание: пустой массив зависимостей [] означает, что
      // этот useEffect будет запущен один раз
      // аналогично componentDidMount()
      useEffect(() => {
        fetch("http://127.0.0.1:8000/api/categories/", {
            method: "GET",
          })
          .then(res => res.json())
          .then(
            (result) => {
              setIsLoaded(true);
              setItems(result);
            },
            // Примечание: важно обрабатывать ошибки именно здесь, а не в блоке catch(),
            // чтобы не перехватывать исключения из ошибок в самих компонентах.
            (error) => {
              setIsLoaded(true);
              setError(error);
            }
          )
      }, []);
      var renderedData;
      if (error) {
        renderedData = <div>Ошибка: {error.message}</div>;
      } else if (!isLoaded) {
        renderedData = <div>Загрузка...</div>;
      } else {
        renderedData = (
          <>
            {items.map(item => (
              <CategoryCard name={item.name} image={item.image_url} id={item.id}>
              </CategoryCard>
            ))}
          </>
        );
      }
    return (
        <>
        <PageBar text={"Все категроии"}/>
        <div className='px-[375px] h-[122px] font-roboto text-4xl flex items-center'>
            Категории товаров
        </div>
        <div className='px-[375px] flex flex-row flex-wrap justify-between gap-y-2'>
            {renderedData}
        </div>
        </>
    );
}

export default Categories;